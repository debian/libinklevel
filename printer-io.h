/* printer-io.h
 *
 * (c) 2014, 2015, 2018, 2022 Markus Heinz
 *
 * This software is licensed under the terms of the GPL.
 * For details see file COPYING.
 */

#ifndef PRINTER_IO_H
#define PRINTER_IO_H

#include "config.h"

#include <libusb.h>
#include "libusb-utils.h"

#include <stddef.h>
#include <stdint.h>

/* Return codes */

#define PRINTER_SUCCESS 1
#define PRINTER_FAILURE 0

/* Parameter for mode in printer_descriptor */

#define PRINTER_MODE_LIBUSB 1
#define PRINTER_MODE_DEVICE 2

typedef struct printer_descriptor {
  // Defining the connection
  int port; // Type of port
  const char *device_file; // Name of device node
  int portnumber; // instance number of printer port
  int mode; // PRINTER_MODE_LIBUSB or PRINTER_MODE_DEVICE

  // Storing the connection
  int fd; // File descriptor

  usb_printer *printer; // USB printer descriptor
} printer_descriptor;

int open_wrapper(struct printer_descriptor *desc);
int close_wrapper(struct printer_descriptor *desc);

int read_wrapper(struct printer_descriptor *desc, char *buffer, size_t bufsize,
		 int *transfered, int nonblocking);
int write_wrapper(struct printer_descriptor *desc, char *buffer, 
		  size_t bufsize, int *transfered);

#endif
