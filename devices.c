/* devices.c
 *
 * (c) 2015, 2018, 2022 Markus Heinz
 *
 * This software is licensed under the terms of the GPL.
 * For details see file COPYING.
 */

#include "config.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>

#include "inklevel.h"
#include "internal.h"
#include "devices.h"
#include "bjnp.h"
#include "libusb-utils.h"

int get_device_id(const int port, const char *device_file, 
                  const int portnumber, char *device_id) {
  int result = COULD_NOT_GET_DEVICE_ID;

  if (port == USB) {
    usb_printer *printer = NULL;

    if (init_usb() != USB_SUCCESS) {
      return COULD_NOT_GET_DEVICE_ID;
    }

    printer = find_printer(portnumber);

    if (printer != NULL) {
      result = open_device_handle(printer);

      if (result == USB_SUCCESS) {
	result = get_usb_device_id(printer, device_id, BUFLEN);
        release_device_handle(printer);
      }
      
      free(printer);
    }

    shutdown_usb();

    if (result == USB_SUCCESS)
      return OK;
    else
      return COULD_NOT_GET_DEVICE_ID;

  } else if (port == CUSTOM_BJNP)  {
    return bjnp_get_id_from_named_printer(portnumber, device_file, device_id);
  } else if (port == BJNP) {
    return bjnp_get_id_from_printer_port(portnumber, device_id);
  } else {
    return UNKNOWN_PORT_SPECIFIED;
  }
}
