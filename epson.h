/* epson.h
 *
 * (c) 2009, 2014 Markus Heinz
 *
 * This software is licensed under the terms of the GPL.
 * For details see file COPYING.
 */

#ifndef EPSON_H
#define EPSON_H

int get_ink_level_epson(const int port, const char *device_file,
			const int portnumber, struct ink_level *level);
#endif
